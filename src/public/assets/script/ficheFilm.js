$(document).ready(function() {
    $(function() {
        $("#datepicker").datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            showOn: "button",
            buttonImage: "/assets/img/lnr-calendar-full.svg",
            buttonImageOnly: true,
            dateFormat: 'dd-mm-yy',
            minDate: new Date(),
            maxDate: "+2W",
            onSelect: function(dateText) {
                var dateData = dateText.split('-');

                var year  = dateData[2];
                var month = dateData[1];
                var day   = dateData[0];

                var formattedDate = year + '-' + month + '-' + day;

                $('.art-time-select').each(function (index, node) {
                    var url = $(node).attr('href');

                    url = url.replace('yyyy-mm-dd', formattedDate);

                    $(node).attr('href', url);
                    $(node).removeClass('disabled');
                });
            }
        });
    });

    $(document).on('click', '.art-time-select.disabled', function (e) {
        e.preventDefault();

        alert('Veuillez sélectionner une date.');
    });

    $.datepicker.setDefaults({
        altField: "#datepicker",
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.'
    });

    $("#lienHoraires").click(function() {
        $("html, body").animate({
            scrollTop: $("#sectionHoraires").offset().top
        }, 2000)
    })
});
