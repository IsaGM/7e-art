$(document).ready(function() {

    function getSelectValue(selectId) {
        /**On récupère l'élement html <select>*/
        var selectElmt = document.getElementById(selectId);
        /**
        selectElmt.options correspond au tableau des balises <option> du select
        selectElmt.selectedIndex correspond à l'index du tableau options qui est actuellement sélectionné
        */

        return selectElmt.options[selectElmt.selectedIndex].value;
    }
});

//Calcul du prix en fonction du nombre de place sélectionnées pour l'achat
$( document ).change(function() {
    var ticketPrice;
    ticketPrice = $('#quantitePlace option:selected').text() * parseInt(9);
    $("#ticketPrice").html(ticketPrice);
});

//module de confirmation apres paiement
$(document).on('click', '.payed', function () {
    alert('Votre achat est bien enregistré');
});