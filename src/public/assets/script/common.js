$(document).ready(function () {
   $(document).on('submit', 'form#newsletterForm', function (e) {
       e.preventDefault();

       var form = $(this);
       var url  = form.attr('action');

       $.ajax({
           url: url,
           data: form.serialize(),
           type: 'POST',
           dataType: 'json',
           success: function (response) {
               if (response.success) {
                   $('form#newsletterForm input[type=email]').val('');
                   alert('Votres inscription à la newsletter a bien été prise en compte.');
               }
           }
       });
   });
});