$(document).ready(function() {
    $('#carouselFilmsAccueil').carousel();
    $('#carouselFilmsAccueil').on('slide.bs.carousel', function(e) {

console.log('là');
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 6;
        var totalItems = $('.carousel-item').length;

        if (idx >= totalItems - (itemsPerSlide - 1)) {
            var it = itemsPerSlide - (totalItems - idx);
            for (var i = 0; i < it; i++) {
                // append slides to end
                if (e.direction == "left") {
                    $('.carousel-item').eq(i).appendTo('.carousel-inner');
                } else {
                    $('.carousel-item').eq(0).appendTo('.carousel-inner');
                }
            }
        }
    });

    $('#btn-test').click(function(){
        alert('ici');
        $('#carouselFilmsAccueil').carousel('next');
    });
});


// JS de la nav fixe

jQuery(document).ready(function() {

    var navOffset = jQuery("nav").offset().top;

    jQuery("nav").wrap('<div class="nav-placeholder"></div>');
    jQuery(".nav-placeholder").height(jQuery("nav").outerHeight());

    jQuery("nav").wrapInner('<div class="nav-inner"></div>');
    jQuery("nav-inner").wrapInner('<div class="nav-inner-most"></div>');

    jQuery(window).scroll(function() {

        var scrollPos = jQuery(window).scrollTop();

        if(scrollPos >= navOffset) {
            jQuery("nav").addClass("fixe");
        } else {
            jQuery("nav").removeClass("fixe");
        }
    });

});