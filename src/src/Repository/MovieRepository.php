<?php

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Movie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Movie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Movie[]    findAll()
 * @method Movie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MovieRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    public function findByDateRelease(\DateTime $dateRelease )
    {
        $now = new \DateTime();

        return $this->createQueryBuilder('m')
            ->andWhere('m.dateRelease >= :lastWeek')
            ->andWhere('m.dateRelease <= :now')
            ->setParameter('lastWeek', $dateRelease->format('Y-m-d'))
            ->setParameter('now', $now->format('Y-m-d'))
            ->orderBy('m.dateRelease', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByNextRelease(\DateTime $dateRelease )
    {
        $now = new \DateTime();

        return $this->createQueryBuilder('m')
            ->andWhere('m.dateRelease <= :nextWeek')
            ->andWhere('m.dateRelease > :now')
            ->setParameter('nextWeek', $dateRelease->format('Y-m-d'))
            ->setParameter('now', $now->format('Y-m-d'))
            ->orderBy('m.dateRelease', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }


    // /**
    //  * @return Movie[] Returns an array of Movie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function search($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.title LIKE :val')
            ->orWhere('m.directors LIKE :val')
            ->orWhere('m.actors LIKE :val')
            ->orWhere('m.genres LIKE :val')
            ->setParameter('val', '%' . $value . '%')
            ->getQuery()
            ->getResult()
        ;
    }
}
