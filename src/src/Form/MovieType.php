<?php

namespace App\Form;

use App\Entity\Movie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class MovieType extends AbstractType
{
    //Création du formulaire pour l'ajout d'un nouveau film
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'productId',
                TextType::class,
                [
                    'label' => 'Id produit'
                ]
            )
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'Titre'
                ]
            )
            ->add(
                'dateRelease',
                DateType::class,
                [
                    'label' => 'Date de sortie'
                ]
            )
            ->add(
                'directors',
                TextType::class,
                [
                    'label' => 'Réalisateurs'
                ]
            )
            ->add(
                'countries',
                TextType::class,
                [
                    'label' => 'Origines'
                ]
            )
            ->add(
                'actors',
                TextType::class,
                [
                    'label' => 'Acteurs'
                ]
            )
            ->add(
                'genres',
                TextType::class,
                [
                    'label' => 'Genres'
                ]
            )
            ->add(
                'storyline',
                TextareaType::class,
                [
                    'label' => 'Synopsis'
                ]
            )
            ->add(
                'posters',
                TextType::class,
                [
                    'label' => 'Lien d\'affiche'
                ]
            )
            ->add(
                'trailer',
                TextType::class,
                [
                    'label' => 'Lien de bande-annonce'
                ]
            )
            ->add(
                'length',
                TextType::class,
                [
                    'label' => 'Durée du film (en seconde)'
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movie::class,
        ]);
    }
}
