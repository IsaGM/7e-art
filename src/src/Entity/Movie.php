<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * Création et liaison avec la base de donnée sur la table "movie"
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=10, name="productId")
     */
    private $productId;

    /**
     * @ORM\Column(type="string", length=91, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", name="dateRelease")
     */
    private $dateRelease;

    /**
     * @ORM\Column(type="string", length=76)
     */
    private $directors;

    /**
     * @ORM\Column(type="string", length=61)
     */
    private $countries;

    /**
     * @ORM\Column(type="string", length=820, nullable=true)
     */
    private $actors;

    /**
     * @ORM\Column(type="string", length=43, nullable=true)
     */
    private $genres;

    /**
     * @ORM\Column(type="string", length=1902, nullable=true)
     */
    private $storyline;

    /**
     * @ORM\Column(type="text")
     */
    public $posters;

    /**
     * @ORM\Column(type="string", length=95, name="trailerSubtitled", nullable=true)
     */
    private $trailerSubtitled;

    /**
     * @ORM\Column(type="string", length=95, nullable=true)
     */
    private $trailer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $length;

    /**
     * @return mixed
     */
    public function getDateRelease()
    {
        return $this->dateRelease;
    }

    /**
     * @param mixed $dateRelease
     * @return Movie
     */
    public function setDateRelease($dateRelease)
    {
        $this->dateRelease = $dateRelease;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     * @return Movie
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }



    /**
     * @return mixed
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * @param mixed $directors
     * @return Movie
     */
    public function setDirectors($directors)
    {
        $this->directors = $directors;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param mixed $countries
     * @return Movie
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * @param mixed $actors
     * @return Movie
     */
    public function setActors($actors)
    {
        $this->actors = $actors;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * @param mixed $genres
     * @return Movie
     */
    public function setGenres($genres)
    {
        $this->genres = $genres;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStoryline()
    {
        return $this->storyline;
    }

    /**
     * @param mixed $storyline
     * @return Movie
     */
    public function setStoryline($storyline)
    {
        $this->storyline = $storyline;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosters()
    {
        return $this->posters;
    }

    /**
     * @param mixed $posters
     * @return Movie
     */
    public function setPosters($posters)
    {
        $this->posters = $posters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrailerSubtitled()
    {
        return $this->trailerSubtitled;
    }

    /**
     * @param mixed $trailerSubtitled
     * @return Movie
     */
    public function setTrailerSubtitled($trailerSubtitled)
    {
        $this->trailerSubtitled = $trailerSubtitled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

    /**
     * @param mixed $trailer
     * @return Movie
     */
    public function setTrailer($trailer)
    {
        $this->trailer = $trailer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     * @return Movie
     */
    public function setLength($length)
    {
        $this->length = $length;
        return $this;
    }


}
