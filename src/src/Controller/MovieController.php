<?php

namespace App\Controller;

use App\Entity\Movie;
use Doctrine\ORM\Query\AST\Functions\CurrentDateFunction;
use DoctrineExtensions\Query\Mysql\Now;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MovieController
 * @package App\Controller
 * @Route("/movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/sorties")
     * affichage des films sur la page des séances de la semaine
     * à partir d'aujourd'hui à la semaine précédente
     */
    public function weekmovie(Request $request)
    {
//          $currentdate = time();
        $currentdate = new \DateTime(); //date d'aujourd'hui

        $lastWeek = $currentdate->add(\DateInterval::createFromDateString('-8 days'));

          //$lastweek = time() - (7 * 24 * 60 * 60);
//        $lastweek = date('Y-m-d', strtotime('-1 week'));
//        $week = $currentdate(+7days);
//        $date = date('Y-m-d',strtotime("- 1 weeks"))
//        $interval = date_diff($currentdate, $lastweek);

        $repository = $this->getDoctrine()->getRepository(Movie::class);

        $movies = $repository->findByDateRelease($lastWeek);

        return $this->render('front/movie/weekmovie.html.twig',
            [
                'movies' => $movies
            ]);
    }

    /**
     * @Route("/prochainement")
     * affichage des films sur la page des séances de la semaine
     * à partir d'aujourd'hui à la semaine suivante
     */
    public function nextmovie()
    {
        $currentdate = new \DateTime(); //date d'aujourd'hui
        $nextWeek = $currentdate->add(\DateInterval::createFromDateString('+1 week'));

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Movie::class);
        $movies = $repository->findByNextRelease($nextWeek);

        return $this->render('front/movie/nextmovie.html.twig',
            [
                'movies' => $movies
            ]);
    }

    /**
     * @Route("/search")
     * affichage des films sur la page des séances de la semaine
     * à partir d'aujourd'hui à la semaine précédente
     */
    public function search(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);

        if ($request->get('_q', false)) {
            $movies = $repository->search($request->get('_q'));
        } else {
            $movies = $repository->findAll();
        }

        return $this->render('front/movie/search.html.twig', [
            'movies' => $movies
        ]);
    }



    /**
     * @Route("/{id}")
     * Affichage des informations d'un film selon l'id
     */
    public function infomovie(Movie $movie)
    {
        return $this->render('front/movie/infomovie.html.twig',
            [
                'movie' => $movie
            ]);
    }
}
