<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Movie;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AppController
 * @package App\Controller
 * @Route("/")
 */
class AppController extends Controller
{
    /**
     * @Route("/")
     * Affichage des articles et des films sur le homepage
     */
    public function index()
    {
        $currentdate = new \DateTime(); //date d'aujourd'hui
        $lastWeek = $currentdate->add(\DateInterval::createFromDateString('-8 days'));
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Article::class);
        $articles = $repository->findBy([], ['publicationDate' => 'desc'], 2);
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Movie::class);
        $movies = $repository->findByDateRelease($lastWeek);
        return $this->render(
            'front/index.html.twig',
            [
                'articles' => $articles,
                'movies' => $movies
            ]
        );
    }
}
