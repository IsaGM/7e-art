<?php

namespace App\Controller;

use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReservationController
 * @package App\Controller
 * @Route("/reservation")
 */
class ReservationController extends AbstractController
{

    /**
     * @Route("/movie/{id}/{date}/{time}/{lang}")
     */
    public function index(Movie $movie, $date, $time, $lang )
    {
        $bookingDate = new \Datetime($date . 'T' . $time);

        return $this->render('front/reservation/index.html.twig',  [
                'movie'       => $movie,
                'bookingDate' => $bookingDate,
                'lang' => $lang
            ]
        );
    }
}
