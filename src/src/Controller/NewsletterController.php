<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Newsletter;

/**
 * Class NewsletterController
 * @package App\Controller
 * @Route("/newsletter")
 */
class NewsletterController extends AbstractController
{
    /**
     * @Route("/", methods={"POST"})
     */
    public function index(Request $request)
    {
        $newsletter = new Newsletter();
        $newsletter->setEmail($request->get('email'));

        $em = $this->getDoctrine()->getManager();

        $em->persist($newsletter);
        $em->flush();

        return new JsonResponse([
            'success' => true
        ]);
    }

}
