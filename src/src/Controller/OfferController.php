<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OfferController
 * @package App\Controller
 * @Route("/offer")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/")
     * Affichage de la pages des offres
     */
    public function index()
    {
        return $this->render('front/offer/index.html.twig');
    }

    /**
     * @Route("/purchase1")
     * Affichage de la 1ère offre
     */
    public function purchase1()
    {
        return $this->render('front/offer/purchase1.html.twig');
    }

    /**
     * @Route("/purchase2")
     * Affichage de la 2ème offre
     */
    public function purchase2()
    {
        return $this->render('front/offer/purchase2.html.twig');
    }

    /**
     * @Route("/purchase3")
     * Affichage de la 3ème offre
     */
    public function purchase3()
    {
        return $this->render('front/offer/purchase3.html.twig');
    }
}


