<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 11/12/2018
 * Time: 15:52
 */

namespace App\Controller\Admin;


use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @package App\Controller\Admin
 * @Route("/admin/contact")
 */
class ContactController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function index()
    {
        /*
         * faire la page qui liste les articles dans un tableau html
         * avec nom de la catégorie
         * nom de l'auteur
         * et date au format français
         * (tous les champs sauf le contenu
         */
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Contact::class);
        $messages = $repository->findBy([], ['id' => 'desc']);

        return $this->render(
            'admin/contact/index.html.twig',
            [
                'messages' => $messages
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(Contact $message)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($message);
        $em->flush();

        $this->addFlash(
            'success',
            'Le message a été supprimé.'
        );

        return $this->redirectToRoute(
            'app_admin_contact_index'
        );
    }
}