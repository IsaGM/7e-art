<?php
/**
 * Created by PhpStorm.
 * User: isabelle
 * Date: 2018-12-17
 * Time: 12:49
 */

namespace App\Controller\Admin;

use App\Entity\Newsletter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsletterController
 * @package App\Controller\Admin
 * @Route("/admin/newsletter")
 */
class NewsletterController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Newsletter::class);
        $emails = $repository->findBy([], ['id' => 'desc']);

        return $this->render(
            'admin/newsletter/index.html.twig',
            [
                'email' => $emails
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(Newsletter $email)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($email);
        $em->flush();

        $this->addFlash(
            'success',
            'L\'email a été supprimé.'
        );

        return $this->redirectToRoute(
            'app_admin_newsletter_index'
        );
    }
}