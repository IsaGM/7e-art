<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\User;
use App\Form\UserType;

class SecurityController extends AbstractController
{
    /**
     * @Route("/admin")
     */
    public function index()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_admin_movie_index');
        }

        return $this->redirectToRoute('app_admin_security_login');
    }

    /**
     * @Route("/inscription")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // encode le mot de passe, à partir de la config "encoders" de security.yaml
                $password = $passwordEncoder->encodePassword(
                    $user,
                    $user->getPlainPassword()
                );

                $user->setPassword($password);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Votre compte a été créé');

                return $this->redirectToRoute('app_admin_index');

            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'admin/security/register.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/connexion")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // traitement du formulaire par Security
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if (!empty($error)) {
            $this->addFlash('error', 'Identifiants incorrects');
        }

        return $this->render(
            'admin/security/login.html.twig',
            [
                'last_username' => $lastUsername
            ]
        );
    }

    /**
     * @Route("/admin/user/{id}/delete")
     */
    public function deleteUser($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);
        //        objet User dont l'id en bdd est celui reçu dans l'url
        $user = $repository->find($id);

//        si l'id existe en bdd
        if (!is_null($user)) {
//            suppression de l'utilisateur en bdd
            $em->remove($user);
            $em->flush();

            $this->addFlash(
                'success',
                'L\'utilisateur a été supprimé.'
            );

            return $this->redirectToRoute(
                'app_admin_security_listusers'
            );
        }
    }

    /**
     * @Route("/admin/user/{id}/promote")
     */
    public function promote(User $user)
    {
        $user->setRole('ROLE_ADMIN');

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_admin_security_listusers');
    }

    /**
     * @Route("/admin/user/{id}/demote")
     */
    public function demote(User $user)
    {
        $user->setRole('ROLE_USER');

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_admin_security_listusers');
    }

    /**
     * @Route("/admin/user/list")
     */
    public function listUsers()
    {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository(User::class);
//        retourne tous les utilisateurs de la table user en bdd sous la forme d'un tableau d'objets User
        $users = $repository->findAll();

        dump($users);

        return $this->render(
            'admin/user/user.html.twig',
            [
                'users'       => $users
            ]
        );
    }
}
