<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Form\ArticleType;
use ProxyManager\Generator\Util\UniqueIdentifierGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

    /**
     * Class ArticleController
     * @package App\Controller\Admin
     *
     * @Route("/admin/article")
     */
class ArticleController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        /*
         * faire la page qui liste les articles dans un tableau html
         * avec nom de la catégorie
         * nom de l'auteur
         * et date au format français
         * (tous les champs sauf le contenu
         */
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Article::class);
        $articles = $repository->findBy([], ['publicationDate' => 'desc']);

        return $this->render(
            'admin/article/index.html.twig',
            [
                'articles' => $articles
            ]
        );
    }

    /**
     * {id} est optionnel et doit être un nombre
     * @Route("/edition/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $originalImage = null;

        if (is_null($id)){ // création
            $article = new Article();
        } else { // modification

            $article = $em->find(Article::class, $id);

            if (!is_null($article->getImage())){
//                nom du fichier venant de la bdd
                $originalImage = $article->getImage();
//                on sette l'image avec un objet File pour le traitement par le formulaire
                $article->setImage(
                    new File($this->getParameter('upload_dir') . $originalImage)
                );
            }

//            404 si l'id reçu dans l'url n'est pas en bdd
            if (is_null($article)){
                throw new NotFoundHttpException();
            }
        }

//        création du formulaire lié à l'article
        $form = $this->createForm(ArticleType::class, $article);
//        le formulaire analyse la requête HTTP et traite le formulaire s'il a été soumis
        $form->handleRequest($request);

//        si le formulaire a été envoyé
        if ($form->isSubmitted()) {
//            dump($article);

//            si les validations à partir des annotations dans l'entité Article sont ok
            if ($form->isValid() ) {
                /**
                 * @var UploadedFile $image
                 */
                $image = $article->getImage();

//                s'il y a eu une image uploadée
                if (!is_null($image)){
//                    nom de l'image dans notre application
                    $filename = uniqid() . '.' . $image->guessExtension();

//                    équivalent de move_uploaded_files()
                    $image->move(
//                        répertoire de destination
//                        cf. le paramètre upload_dir dans config/services.yaml
                        $this->getParameter('upload_dir'),
//                        nom du fichier
                        $filename
                    );
//                    on sette l'attribut image de l'article avec le nom de l'image pour l'enregistrement en bdd
                    $article->setImage($filename);

//                    en modification, on supprime l'ancienne image s'il y en a une
                    if (!is_null($originalImage)) {
                        unlink($this->getParameter('upload_dir') . $originalImage);
                    }
                } else {
//                       sans upload, pour la modification, on sette l'attribut
//                       image avec le nom de l'ancienne image
                    $article->setImage($originalImage);
                }
//                enregitrement de l'article en bdd
                $currentDate = new \DateTime();

                if (is_null($id)){
                    $article->setPublicationDate($currentDate);
                }

                $em->persist($article);
                $em->flush();

//                message de confirmation
                $this->addFlash('success', 'L\'article a été créé');
//                redirection vers la liste
                return $this->redirectToRoute('app_admin_article_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs.');
            }
        }

        return $this->render(
            'admin/article/edit.html.twig',
            [
//                passage du formulaire au template
                'form' => $form->createView(),
                'original_image' => $originalImage
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(Article $article)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($article);
        $em->flush();

        $this->addFlash(
            'success',
            'L\'article a été supprimé.'
        );

        return $this->redirectToRoute(
            'app_admin_article_index'
        );
    }
}
