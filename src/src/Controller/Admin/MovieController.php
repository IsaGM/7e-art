<?php

namespace App\Controller\Admin;


use App\Entity\Movie;
use App\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovieController
 * @package App\Controller
 * @Route("/admin/movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Movie::class);
        $movies = $repository->findAll();

        return $this->render(
            'admin/movie/index.html.twig',
            [
                'movies' => $movies
            ]
        );
    }

    /**
     * @Route("/edit")
     */
    public function edit(Request $request)
    {
        $movie = new Movie();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $em->persist($movie);
                $em->flush();

                $this->addFlash('success','Votre film a bien été créé');
                return $this->redirectToRoute('app_admin_movie_index');
            }
        }
        return $this->render(
            'admin/movie/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/modify/{id}", defaults={"id": null}, requirements={"id":"\d+"})
     */
    public function modify(Request $request, $id)
    {
        dump($id);
        $em = $this->getDoctrine()->getManager();

        if(is_null($id)){ //création
            $movie = new Movie();
        }else {// modification
            $movie = $em->find(Movie::class, $id);
            }
            // 404 si l'id recu dans l'url n'est pas en BDD
            if (is_null($movie)){
                throw new NotFoundHttpException();
            }
//        création du formulaire lié à la catégorie
        $form = $this->createForm(MovieType::class, $movie);
//        le formulaire analyse le requête HTTP
//        et traite le formulaire s'il a été soumis
        $form->handleRequest($request);
//        Si le formulaire a été envoyé
        if($form->isSubmitted()){
            dump($movie);
//        si les validations à partir des annotations dans
//        l'entité Category sont ok
            if($form->isValid()){
//                enregistrement de la catégorie en bdd
                $em->persist($movie);
                $em->flush();

//                message de confirmation
                $this->addFlash('success','La catégorie est créée');
//                redirection vers la liste
                return $this->redirectToRoute('app_admin_movie_index');
            } else {
                $this->addFlash('error', 'Le Formulaire contient des erreurs');
            }
        }
        return $this->render(
            'Admin/movie/edit.html.twig',
            [
//                passage du formulaire au template
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/delete-movie/{id}")
     */
    public function deleteMovie($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(Movie::class);
//        objet User dont l'id en bdd est celui reçu dans l'url
        $movie = $repository->find($id);

//        si l'id existe en Bdd
        if (!is_null($movie)){
//            suppression du film en bdd
            $em->remove($movie);
            $em->flush();

            $this->addFlash('success','Le film a été supprimé');
            return $this->redirectToRoute('app_admin_movie_index');

        } else {
            $this->addFlash('error','Le film n\'a pas été supprimé');
        }
    }
}
