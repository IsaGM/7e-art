<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @package App\Controller
 * @Route("/contact")
 */
class ContactController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = new Contact();

//        création du formulaire lié à l'article
        $form = $this->createForm(ContactType::class, $contact);
//        le formulaire analyse la requête HTTP et traite le formulaire s'il a été soumis
        $form->handleRequest($request);

//        si le formulaire a été envoyé
        if ($form->isSubmitted()) {

//            si les validations à partir des annotations dans l'entité Article sont ok
            if ($form->isValid() ) {
                $em->persist($contact);
                $em->flush();
                //                message de confirmation
                $this->addFlash('success', 'Votre message a été envoyé');
//                redirection vers la liste
                return $this->redirectToRoute('app_contact_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs.');
            }
        }

        return $this->render(
            'front/contact/contact.html.twig',
            [
//                passage du formulaire au template
                'form' => $form->createView(),
            ]
        );
    }
}
