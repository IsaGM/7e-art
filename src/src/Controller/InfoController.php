<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class InfoController
 * @package App\Controller
 * @Route("/info")
 */
class InfoController extends AbstractController
{
    /**
     * @Route("/montpellier")
     */
    public function index()
    {
        return $this->render('/front/info/salle_cinema_montpellier.html.twig');
    }

    /**
     * @Route("/dijon")
     */
    public function dijon()
    {
        return $this->render('/front/info/salle_cinema_dijon.html.twig');
    }

    /**
     * @Route("/lyon")
     */
    public function lyon()
    {
        return $this->render('/front/info/salle_cinema_lyon.html.twig');
    }

    /**
     * @Route("/marseille")
     */
    public function marseille()
    {
        return $this->render('/front/info/salle_cinema_marseille.html.twig');
    }

    /**
     * @Route("/nancy")
     */
    public function nancy()
    {
        return $this->render('/front/info/salle_cinema_nancy.html.twig');
    }

    /**
     * @Route("/rennes")
     */
    public function rennes()
    {
        return $this->render('/front/info/salle_cinema_rennes.html.twig');
    }

    /**
     * @Route("/strasbourg")
     */
    public function strasbourg()
    {
        return $this->render('/front/info/salle_cinema_strasbourg.html.twig');
    }

    /**
     * @Route("/toulouse")
     */
    public function toulouse()
    {
        return $this->render('/front/info/salle_cinema_toulouse.html.twig');
    }

}
